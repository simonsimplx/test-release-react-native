module.exports = {
  branches: ["+([0-9])?(.{+([0-9]),x}).x", "main", "next", "next-major"],
  plugins: [
    "@semantic-release/commit-analyzer",
    "@semantic-release/release-notes-generator",
    [
      "@semantic-release/changelog",
      {
        changelogFile: "CHANGELOG.md",
      },
    ],
    ["@semantic-release/npm"],
    [
      "@semantic-release/exec",
      {
        publishCmd: "cp -r . ../copy && mv ../copy ./dist",
        addChannelCmd:
          'echo "VERSION=${nextRelease.version}" >> release.env && echo "CHANNEL=${nextRelease.channel}" >> release.env && echo "VERSION_MAJOR=$(echo ${nextRelease.version} | cut -d . -f 1)" >> release.env && echo "VERSION_MINOR=$(echo ${nextRelease.version} | cut -d . -f 2)" >> release.env',
        successCmd:
          'echo "VERSION=${nextRelease.version}" >> release.env && echo "CHANNEL=${nextRelease.channel}" >> release.env && echo "VERSION_MAJOR=$(echo ${nextRelease.version} | cut -d . -f 1)" >> release.env && echo "VERSION_MINOR=$(echo ${nextRelease.version} | cut -d . -f 2)" >> release.env',
      },
    ],
  ],
};
